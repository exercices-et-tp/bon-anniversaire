# Le projet _BonAnniversaire_

## Préambule 

Ce projet est un exercice dans le cadre du cours sur les tests unitaire situé à l'adresse :
https://giloop-projects.gitlab.io/cours-giloop/02.web/js/tests-unitaires/

Auteur : [Gilles Gonon](https://gilles.gonon.free.fr) 

## Présentation 

Le cahier des charges a été exprimé par le client comme suit : 

> Je souhaite avoir une application dans laquelle je puisse rentrer les dates anniversaires de mes proches et qui me donne leur âge 
> en les triant par date d'anniversaire la plus proche. Les anniversaires très proches ( la veille ou le jour même) doivent être mis en avant. 
> Un plus serait d'avoir des notifications.

## Conception 

Les architectes _agiles_ ont défini un MVP (minimum viable product) et les sprints suivants : 

### Sprint 1 (MVP)

- L'application est contenue dans 1 page HTML, utilisant le Framework Bootstrap
- Les données sont stockées par l'utilisateur en local avec le ``localstorage`` du navigateur et rechargées avec la page
- Les anniversaires sont affichés dans des _cartes_ triées par ordre chronologique
- Une fenêtre modale permet la saisie d'une nouvelle personne
- Une classe _Personne_ contient 
  - les propriétés ``{id:"xxx", nom:"Name", dateAnniv:objet Date, comment:"commentaire"}``
  - les méthodes pour obtenir l'âge de la personne et le nombre de jour avant anniversaire
- Une classe _MesAnniversaires_ contient 
  - les propriétés _nom_ et un tableau _arPersonne_ de _Personnes_.
  - les méthodes pour ajouter / modifier / supprimer / afficher la liste des personnes et sauver / charger l'objet dans le localStorage.

### Sprint 2 

- Ajouter l'édition des entrées
- Comme on ne connait pas toujours l'année de naissance, il faut la rendre optionnelle
- On souhaite aussi afficher les anniversaires jusqu'à 3 jours après leur date
- Ajouter l'édition des entrées

### Sprint 3 

- Ajouter une recherche rapide / filtre par nom (ou par date ?)
- Ajout d'une photo des personnes
- Ajouter l'éphéméride en récupérant l'API https://github.com/theofidry/ephemeris

### Sprint 4

- PWA : Écrire le fichier manifest.json
- Import/export des contacts
- Import depuis un fichier de contacts Google
- Import sélectif depuis un fichier

### Sprint 5

- Internationalisation : traduire l'application dans plusieurs langues (i18n)
- ...

## Exercice

### Prise en main

- Récupérer le code source de l'application _Bon Anniversaire_ en cours de développement à l'adresse :
https://gitlab.com/exercices-et-tp/bon-anniversaire
- Analysez la structure du code de l'application : 
  - La page `index.html` est le point d'entrée de l'application
  - La page `template.html` rassemble uniquement des bribes de codes Html Bootstrap pour tester des mises en page. Utilisez là pour tester votre mise en page personnelle. Ces bouts se retrouvent plus ou moins fidèlement dans l'application.
  - Quelles sont les cripts et classes Javascript, quels sont leurs rôles ?
- Lancez le fichier index.html avec le live server pour avoir un aperçu de l'application : le site est à peu près en place visuellement, mais fonctionnellement presque rien de marche 
- Il semblerait que les développeurs ont préparé les tests de différentes fonctions qui ne passent pas : saurez-vous les retrouver et les corriger ?

### Codage MVP

- Mettre au point les fonctions de la classe `Personne` : `getAge()` et `getJoursAvantAnniv()` pour qu'elles valident les tests unitaires (dossier `test`).
- Modifier la page `index.html` pour que la date soit affichée dans le sous-titre de l'application
- Modifier les cartes pour qu'elles incluent la date de naissance de la personne en plus du jour.

{{% notice tip %}}
Faites une recherche sur le mot clef 'TODO' dans le code permet aussi de voir les choses identifiées comme à faire.
{{% /notice %}}

![Aperçu du MVP](https://giloop-projects.gitlab.io/cours-giloop/images/bon-anniv-mvp.png)
![Tests Jasmine du MVP](https://giloop-projects.gitlab.io/cours-giloop/images/bon-anniv-mvp-tests-jasmine.png))

### Et après ...

Vous pouvez ensuite suivre les étapes proposées du développement ou choisir celles qui vous intéressent pour faire vos Sprints. 

N'oubliez pas de mettre en place les tests AVANT de coder les nouvelles fonctions.

### Solution

Le MVP finalisé se trouve dans la branche `MVP`.  
La suite du développement continue dans la branche `dev` et ne suit plus les sprints présentés ici.
