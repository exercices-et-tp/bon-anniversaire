// Régalge de la configuration du lancement des tests
jasmine.getEnv().configure({
    random: false,
    hideDisabled: true
});

// Pour tester les dates on créé un Mock (simulation) du temps avec Jasmine


// Tests de la classe Personne
describe("Classe Personne, calcul de l'age", function () {
    // Création d'un Mock de la date courante au 14 fev 2020
    var baseTime = new Date(2020, 1, 14); // 14 fev 2020
    jasmine.clock().mockDate(baseTime);

    // Création d'une Personne qui a 20 ans aujourd'hui
    var now = new Date();
    var unGars = new Personne(1, 'Gilles', new Date(now.getFullYear() - 20, now.getMonth(), now.getDate()), 'On a tous les jours 20 ans');
    // Création d'une Personne qui a eu 20 ans le premier Janvier
    var unGarsVieux = new Personne(1, 'Gilles', new Date(now.getFullYear() - 20, 0, 1), 'On a pas tous les jours 20 ans');
    // Création d'une Personne qui aura 20 ans le 31 déc
    var unGarsJeune = new Personne(1, 'Gilles', new Date(now.getFullYear() - 20, 11, 31), 'On a pas tous les jours 20 ans');

    it("doit renvoyer le bon age d'une personne le jour de son anniversaire", function () {
        expect(unGars.getAge()).toBe(20);
    })

    it("doit renvoyer le bon age d'une personne née il y a 20 ans le 1er janvier", function () {
        expect(unGarsVieux.getAge()).toBe(20);
    })

    it("doit renvoyer le bon age d'une personne née il y a 20 ans le 31 décembre", function () {
        // TODO : vérifier que l'on n'est pas le 31 déc
        expect(unGarsJeune.getAge()).toBe(19);
    })

    jasmine.clock().mockDate();
});

describe("Classe Personne, calcul du nombre de jour séparant l'anniversaire", function () {
    // Création d'un Mock de la date courante au 14 fev 2020
    beforeEach(function () {
        var baseTime = new Date(2020, 1, 14); // 14 fev 2020
        jasmine.clock().mockDate(baseTime);
    })
    afterEach(function () {
        jasmine.clock().mockDate();
    })

    // Création d'une Personne qui a 20 ans aujourd'hui   
    var persAnniv = new Personne(1, 'Gilles', new Date(2000, 1, 14), 'J-0 20 ans');
    // Création d'une Personne anniv dans 1 jour (demain)
    var persAnnivDemain = new Personne(1, 'Gilles', new Date(2010, 1, 15), 'J-1 10 ans');
    // Création d'une Personne anniv il y a -1 jour (hier)
    var persAnnivHier = new Personne(1, 'Gilles', new Date(2010, 1, 13), '365');

    it("doit renvoyer 0 le jour de son anniversaire", function () {
        expect(persAnniv.getJoursAvantAnniv()).toBe(0);
    })

    it("doit renvoyer 1 la veille de son anniversaire", function () {
        expect(persAnnivDemain.getJoursAvantAnniv()).toBe(1);
    })

    it("doit renvoyer 365 les années bissextiles le lendemain de son anniversaire", function () {
        expect(persAnnivHier.getJoursAvantAnniv()).toBe(365);
    })


    it("doit renvoyer 364 les années non-bissextiles le lendemain de son anniversaire", function () {
        var baseTime = new Date(2019, 1, 14); // 14 fev 2019
        jasmine.clock().mockDate(baseTime);
        // Création d'une Personne anniv il y a 364 jour (hier)
        var persAnnivHier = new Personne(1, 'Gilles', new Date(2010, 1, 13), '364');

        expect(persAnnivHier.getJoursAvantAnniv()).toBe(364);
    })

});


// Tests de la classe Anniversaire
describe("Classe Anniversaire, vérification du tri par J-anniversaire", function () {
    // Création d'un Mock de la date courante au 1er septembre 2000
    

    var mesAnniv;
    beforeEach(function () {
        var baseTime = new Date(2000, 6, 1);
        jasmine.clock().mockDate(baseTime);
    });

    afterEach(function () {
        // Efface la base temporaire à la fin du test
        mesAnniv.effacerLocal();
        jasmine.clock().mockDate();
    });

    it("doit renvoyer les Personnes triées par nombre jours croissant avant anniversaire", function () {
        // Création d'une liste de Personne à trier dans une base temporaire
        mesAnniv = new Anniversaires('base-test');
        mesAnniv.ajouterPersonne('Pierre', new Date(1970, 7, 1), 'J-31');
        mesAnniv.ajouterPersonne('Jacques', new Date(1950, 5, 27), 'J-361');
        mesAnniv.ajouterPersonne('Paul', new Date(1980, 0, 1), 'J-184');
        mesAnniv.trierPersonnesParJourAvantAnniv();
        var tabNom = mesAnniv.arPersonne.map(el => el.nom);
        expect(tabNom).toEqual(['Pierre', 'Paul', 'Jacques']); // toEqual pour les tableaux deep equal
    })

    it("doit renvoyer les Personnes triées J-0, j-1 et j-364 avant anniversaire", function () {
        // Création d'une liste de Personne à trier dans une base temporaire
        mesAnniv = new Anniversaires('base-test');
        mesAnniv.ajouterPersonne('Pierre', new Date(2000, 5, 30), 'J-364');
        mesAnniv.ajouterPersonne('Jacques', new Date(1999, 6, 2), 'J-1');
        mesAnniv.ajouterPersonne('Paul', new Date(1999, 6, 1), 'J-0');
        mesAnniv.trierPersonnesParJourAvantAnniv();
        var tabNom = mesAnniv.arPersonne.map(el => el.nom);
        expect(tabNom).toEqual([ 'Paul', 'Jacques', 'Pierre']); // toEqual pour les tableaux deep equal
    })


    
});