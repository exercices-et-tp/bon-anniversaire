var mesAnniv = new Anniversaires();
mesAnniv.chargerLocal();
mesAnniv.afficherCartesPersonnes();

// mesAnniv.ajouterPersonne('Gilles', new Date(1974,12,15), 'yes');
// var gil = new Personne(1, 'Gilles', new Date(1974,11,15), 'ok')
var form = document.getElementById('ajoutPersonne');

form.addEventListener('submit', function(event) {
  if (form.checkValidity() === false) {
    event.preventDefault();
    event.stopPropagation();
  }
  form.classList.add('was-validated');
}, false);


const elNom = document.getElementById('nom');
const elDate = document.getElementById('anniversaire');
const elComment = document.getElementById('comment');

function ajouterAnniv() {

    // Validation du formulaire
    var nom = elNom.value; // $('#nom').val();
    var dateA = elDate.value; // $('#anniversaire').val();
    var comment = elComment.value; // $('#comment').val();

    elNom.classList.remove('is-invalid');
    elDate.classList.remove('is-invalid');
    elComment.classList.remove('is-invalid');
    
    // console.log('ajouterAnniv:'+elNom.value+' '+elDate.value+' '+elComment.value);
    
    // Vérification des champs obligatoires
    var bValid = true;
    if(nom.trim() == '' ){
      elNom.classList.add('is-invalid');
      bValid = false;
    }
    
    if(dateA.trim() == '' ){
		  elDate.classList.add('is-invalid');
      bValid = false;
    } 

    if (bValid) {
      // Ajoute la personne
      console.log('valid')
      arDate = dateA.split('-').map(el => Number(el));
      mesAnniv.ajouterPersonne(nom, new Date(arDate[0], arDate[1]-1, arDate[2]), comment.trim());
      // MAJ affichage
      mesAnniv.majAffichage();
      // Vidage formulaire
      elNom.value = "";
      elDate.value = "";
      elComment.value = "";
      elNom.classList.remove('is-invalid');
    } else {
      // Message
      console.log('form ko')
    }
}