/* Fonction utilitaire */
function estBissextile(annee) {
  // Algorithme de définition d'une année bissextile
  // https://fr.wikipedia.org/wiki/Ann%C3%A9e_bissextile#R%C3%A8gle_actuelle
  return annee % 400 === 0 || (annee % 100 !== 0 && annee % 4 === 0);
}
// Surcharge du format d'écriture de l'objet Date en JSON
// Nécessaire pour que la date soit bien relue au chargement du localStorage
Date.prototype.toJSON = function () { return this.toUTCString(); }

/* Définition des classes */
class Personne {
    constructor(id, nom, dateAnniversaire, comment) {
      this.id = id;
      this.nom = nom;
      this.dateAnniversaire = dateAnniversaire;
      this.comment = comment;
      return id;
    }
  
    getAge() {
      // Calcule l'age de la personne
      // TODO : déboguer
      const now = new Date();
      return Math.ceil((now - this.dateAnniversaire)/Personne.unAnMillisecondes);
    }

    getJoursAvantAnniv() {
      // renvoie le nombre de jours restants avant l'anniversaire
      // TODO : déboguer
      const now = new Date();
      return Math.floor((this.dateAnniversaire - now)/Personne.unJourMillisecondes);
    }

    static unJourMillisecondes = 1000*60*60*24;
    static unAnMillisecondes = 365*1000*60*60*24;
  
};
  
class Anniversaires {
    constructor(nom = 'mes-anniversaires') {
      this.nom = nom;
      this.arPersonne = [];
    }
  
    ajouterPersonne(nom, dateAnniv, comment) {
      // Ajout d'un Personne avec incrément automatique de l'ID
      if (typeof nom == 'string' && typeof dateAnniv == 'object' 
          && nom.length>0) {
            // Recherche d'une id dispo
            // Méthode 1 : max+1 
            // var id = Math.max(...this.arPersonne.map(obj => obj.id)) + 1;
            // Méthode 2 : 1er id dispo
            var id = 0
            while (this.arPersonne.map(obj => obj.id).indexOf(id)>=0) { id++; }
  
        this.arPersonne.push(new Personne(id, nom, dateAnniv, comment));
        this.sauverLocal();
        return true;
      } else {
        return false;
      }
    }
  
    supprimerPersonneId(id, maj=true) {
      // Suppression d'une Personne en cherchant l'ID
      // TODO : écrire l'appel dans afficherCartesPersonnes (cf code bouton Ajouter)
      var index = this.arPersonne.map(obj => obj.id).indexOf(id);
      if (index>=0) {
        // Supprime un élément à l'indice index
        this.arPersonne.splice(index,1);
        if (maj) { this.majAffichage(); }
        return true;
      } else {
        // index non trouvé
        return false;
      }
    }
    
    modifierPersonneId(id, newnom, newdateAnniv, newComment) {
      // Modification d'une Personne en cherchant l'ID
      // Recherche de l'objet 
      var oPersonne = this.arPersonne.find(obj => obj.id == id);
      if (oPersonne) {
        // Réaffecte le dateAnniv et le nom si ils ne sont pas modifiés
        oPersonne.dateAnniv = newdateAnniv?newdateAnniv:oPersonne.dateAnniv;
        oPersonne.nom = newnom?newnom:oPersonne.nom;
        oPersonne.newComment = newComment?newComment:oPersonne.newComment;
        return true;
      } else {
        // Identifiant de Personne non trouvé
        return false;
      }
    }
 
    trierPersonnesParJourAvantAnniv() {
      // Tri du tableau des personnes par jours avant l'anniversaire croissant
      // TODO : à vérifier      
      this.arPersonne.sort(function (p1, p2) {
        return p1.getAge() - p2.getAge();
      });
    }

    sauverLocal() {
      localStorage.setItem(this.nom, JSON.stringify(this));
    }
  
    chargerLocal() {
      var strAnniv = localStorage.getItem(this.nom)
      // Test de chargement
      if (strAnniv === null) {
        return false;
      } else {
        // Récupération des propriétés de l'objet
        var oAnniv = JSON.parse(strAnniv);
        this.nom = oAnniv.nom;
        // Reconstruction des objets personnes
        oAnniv.arPersonne.forEach(pers => {
          this.ajouterPersonne(pers.nom, new Date(pers.dateAnniversaire), pers.comment);
        })
        return true;
      }
    }
  
    effacerLocal() {
       localStorage.removeItem(this.nom);
    }

    // Fonctions d'affichage
    majAffichage() {
      // Mise à jour de l'affichage des personnes 
      this.supprimerCartesPersonnes();
      this.afficherCartesPersonnes();
    }

    supprimerCartesPersonnes() {
      // Supprime les cartes des personnes dans la page HTML
      const arCards = document.getElementsByClassName('card');
      for (var i = arCards.length - 1; i >= 0; --i) {
        arCards[i].remove();
      }
    }

    afficherCartesPersonnes() {
      // Affiche les cartes des personnes dans la page HTML
      // Si le tableau est vide 
      if (this.arPersonne.length==0) {
        var el = document.createElement('p')
        el.innerHTML = 'Pas encore de Personne dans la collection'
        return;
      } else {
        var listeAnniv = document.getElementById('anniversaires');
        // TODO : ajouter le tri des Personnes ICI à chaque ajout
        
        this.arPersonne.forEach(p => {
            var age = p.getAge();
            var jours = p.getJoursAvantAnniv();    
            // Element à ajouter
            var el = document.createElement('div');
            el.setAttribute('class', 'card w-100 my-3');
            el.innerHTML = `
           <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-8">
                        <div class="card-title d-flex">
                            <svg class="rounded-circle" width="40" height="40">
                                <rect width="40" height="40" style="fill:rgb(${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)})" />
                                <text x="15" y="25" fill="white">${p.nom[0].toUpperCase()}</text>
                            </svg>
                            <h3 class="mx-3">J-${p.getJoursAvantAnniv()} : ${p.nom}</h3>
                        </div>
                        <p class="card-text">Age : ${p.getAge()} ans (${p.dateAnniv})
                            <br>${p.comment}
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4 text-right">
                        <button class="btn text-white bg-perso" onclick="console.log('TODO : modifier(${p.id})">Modifier</button>
                        <button class="btn btn-danger mx-2" onclick="mesAnniv.supprimerPersonneId(${p.id})">Supprimer</button>
                    </div>
                </div>
            </div>`;
            listeAnniv.appendChild(el);

        });
  
        
        return;
      }
    }

  };

